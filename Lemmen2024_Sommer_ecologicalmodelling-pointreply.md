<!--
SPDX-FileCopyrightText:  2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC-BY-NC-4.0
-->

# Point-by-point reply to reviewer comments

We thank both reviewers for their appreciation of our revised version. All minor suggested formatting issues were applied.  We provide below a point-by-point reply to their one remaining comment.

- We updated the reference to Jakeman et al. which has meanwhile been published and changed from Table to Figure.

- We rechecked all links and homogenized formatting.

# Reviewer #2

> My remaining suggestion is to add a 'housekeeping sentence' - perhaps in the last section of the Introduction: Pointing out 'how this paper works' right at the start, that 'must/should/would' will be marked, and that an overwiev figure and a list of abbreviations is available to refer back to, might avoid some frustration for those who are willing but slightly overwhelmed. If you decide against such a sentence, I would still recommend to reference Figure 2 as early as possible.

We agree with the review and pointed both to the table of abbreviations at first occurence and we introduced Figure 2 early to refer back to.
