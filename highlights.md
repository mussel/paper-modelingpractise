<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->
# Research highlights

- You must publish your model software, it is good enough.
- Good Modeling Software Practices are at the center of good practices in modeling, software, and research.
- Minimum practices are version control, distributed redundancy, a collaboration platform, a license,  documentation, readable code, and archiving
- Build up your good practices toolkit one by one.
