% !TEX root = ./Lemmen2024_modelingpractise.tex
%%
%% SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
%% SPDX-FileCopyrightText: 2007-2020 Elsevier Ltd
%% SPDX-License-Identifier: LPPL-1.2
%% SPDX-FileContributor: Carsten Lemmen
%%%%
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%%
%% The list of all files belonging to the 'Elsarticle Bundle' is
%% given in the file `manifest.txt'.
%%
%% Template article for Elsevier's document class `elsarticle'
%% with harvard style bibliographic references

\documentclass[11pt,5p]{elsarticle}

\usepackage{enumitem}
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{4pt}}
\setlist[itemize]{leftmargin=*,labelsep=5.8mm}
\setlist[enumerate]{leftmargin=*,labelsep=4.9mm}

\usepackage[colorlinks=true]{hyperref}
\usepackage{xurl}
\usepackage{calc}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}

$if(graphics)$
 \usepackage{graphicx}
 \makeatletter
 \newsavebox\pandoc@box
 \newcommand*\pandocbounded[1]{% scales image to fit in text height/width
   \sbox\pandoc@box{#1}%
   \Gscale@div\@tempa{\textheight}{\dimexpr\ht\pandoc@box+\dp\pandoc@box\relax}%
   \Gscale@div\@tempb{\linewidth}{\wd\pandoc@box}%
   \ifdim\@tempb\p@<\@tempa\p@\let\@tempa\@tempb\fi% select the smaller of both
   \ifdim\@tempa\p@<\p@\scalebox{\@tempa}{\usebox\pandoc@box}%
   \else\usebox{\pandoc@box}%
   \fi%
 }
% Set default figure placement to htbp
\def\fps@figure{tbp}
\makeatother
$endif$

% Define page style with header on the first page
\fancypagestyle{firstpage}{
  \fancyhf{} % Clear header and footer
  \fancyhead[L]{\includegraphics[height=3cm]{./assets/elsevier-non-solus-new-with-wordmark.pdf}}
  \fancyhead[C]{submitted to Ecological Modelling}
  \fancyhead[R]{\includegraphics[height=3cm]{./assets/X03043800.jpg}}
  \renewcommand{\headrulewidth}{0pt} % Remove header rule
}

$if(logo)$
\usepackage{tikz}
$endif$

$if(csl-refs)$
% definitions for citeproc citations
\NewDocumentCommand\citeproctext{}{}
\NewDocumentCommand\citeproc{mm}{%
\begingroup\def\citeproctext{#2}\cite{#1}\endgroup}
\makeatletter
% allow citations to break across lines
\let\@cite@ofmt\@firstofone
% avoid brackets around text for \cite:
\def\@biblabel#1{}
\def\@cite#1#2{{#1\if@tempswa , #2\fi}}
\makeatother
\newlength{\cslhangindent}
\setlength{\cslhangindent}{1.5em}
\newlength{\csllabelwidth}
\setlength{\csllabelwidth}{3em}
\newenvironment{CSLReferences}[2] % #1 hanging-indent, #2 entry-spacing
{\begin{list}{}{%
	\setlength{\itemindent}{0pt}
	\setlength{\leftmargin}{0pt}
	\setlength{\parsep}{0pt}
	% turn on hanging indent if param 1 is 1
	\ifodd #1
	\setlength{\leftmargin}{\cslhangindent}
	\setlength{\itemindent}{-1\cslhangindent}
	\fi
	% set entry spacing
	\setlength{\itemsep}{#2\baselineskip}}}
{\end{list}}
\newcommand{\CSLBlock}[1]{\hfill\break\parbox[t]{\linewidth}{\strut\ignorespaces#1\strut}}
\newcommand{\CSLLeftMargin}[1]{\parbox[t]{\csllabelwidth}{\strut#1\strut}}
\newcommand{\CSLRightInline}[1]{\parbox[t]{\linewidth - \csllabelwidth}{\strut#1\strut}}
\newcommand{\CSLIndent}[1]{\hspace{\cslhangindent}#1}
$endif$

\journal{$journal$}
$if(doi)$
\doi{$doi$}
$endif$

\begin{document}

\begin{frontmatter}
\thispagestyle{firstpage}


	%% use the tnoteref command within \title for footnotes;
	%% use the tnotetext command for theassociated footnote;
	%% use the fnref command within \author or \affiliation for footnotes;
	%% use the fntext command for theassociated footnote;
	%% use the corref command within \author for corresponding author footnotes;
	%% use the cortext command for theassociated footnote;
	%% use the ead command for the email address,
	%% and the form \ead[url] for the home page:
	%% \title{Title\tnoteref{label1}}
	%% \tnotetext[label1]{}
	%% \author{Name\corref{cor1}\fnref{label2}}
	%% \ead{email address}
	%% \ead[url]{home page}
	%% \fntext[label2]{}
	%% \cortext[cor1]{}
	%% \affiliation{organization={},
	%%            addressline={},
	%%            city={},
	%%            postcode={},
	%%            state={},
	%%            country={}}
	%% \fntext[label3]{}

\title{$title$}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \affiliation[label1]{organization={},
%%             addressline={},
%%             city={},
%%             postcode={},
%%             state={},
%%             country={}}
%%
%% \affiliation[label2]{organization={},
%%             addressline={},
%%             city={},
%%             postcode={},
%%             state={},
%%             country={}}

$for(author)$
\author%
$if(author.affil)$[$author.affil$]$endif$%
{%
%$if(author.orcid)$\href{https://orcid.org/$author.orcid$}{\includegraphics[width=1em]{logo-orcid-eps-converted-to.pdf}}\,$endif$
$author.name$$if(author.email)$\corref{$author.email$}$endif$}
$sep$$endfor$

$for(affiliations)$
$if(affiliations/length)$
\affiliation[$affiliations.num$]{$affiliations.address$}
$endif$
$endfor$

$if(abstract)$
\begin{abstract}
$abstract$
\end{abstract}
$endif$

%%Graphical abstract
$if(graphicalabstract)$
\begin{graphicalabstract}
\includegraphics{$graphicalabstract$}
\end{graphicalabstract}
$endif$

%%Research highlights
$if(highlights)$
\begin{highlights}
$for(highlights)$\item $highlights$
$endfor$
\end{highlights}
$endif$

%--
%% Keywords
%%% keywords here, in the form: keyword \sep keyword
%%% PACS codes here, in the form: \PACS code \sep code
%%% MSC codes here, in the form: \MSC code \sep code
%%% or \MSC[2008] code \sep code (2000 is the default)
$if(keywords)$
\begin{keyword}
$for(keywords)$$keywords$
$sep$\sep $endfor$
\end{keyword}
$endif$

\date{\today}
\end{frontmatter}
$if(logo)$
\begin{tikzpicture}[remember picture,overlay,shift={(current page.north east)}]
%\node[anchor=north east,xshift=-4cm,yshift=-1cm]{\href{https://www.openmodelingfoundation.org}{\includegraphics[width=3cm]{omf_logo.png}}};
%\node[anchor=north east,xshift=-1cm,yshift=-1cm]{\href{https://www.hereon.de}{\includegraphics[width=3cm]{logo-hereon.png}}};
\end{tikzpicture}
$endif$

$body$

\vspace{6pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(acknowledgement)$
\textbf{Acknowledgements: }$acknowledgement$
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(authorcontributions)$
\textbf{Author contributions: }$authorcontributions$
$endif$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$if(conflictsofinterest)$
\textbf{Conflicts of Interest: }$conflictsofinterest$}
$endif$

$if(abbreviations)$
\subsection*{The following abbreviations are used in this manuscript}\label{sec:abbreviations}

\noindent
\begin{tabular}{@{}lp{0.8\columnwidth}}
$for(abbreviations)$
$abbreviations.short$ & $abbreviations.long$ \\
$endfor$
\end{tabular}
$endif$

\end{document}
