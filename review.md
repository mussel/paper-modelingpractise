<!--
SPDX-FileCopyrightText:  2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC-BY-NC-4.0
-->

# Reviewer #1:
The manuscript is much improved and presents a well-researched and cogent set of good practices. There is a formatting issue in the git init; git add manuscript.md; git commit -m "feat: Created manuscript" sequence of example git commands at the end of page 4 / beginning of page 5.

# Reviewer #2:

Dear Authors,
thank you for an even better Manuscript!

Although I feel the Paper is ready to be published as is, I have a few comments and additional minor suggestions:

I really appreciate the new introduction, I think it meets the indented audience where they, actually, are. Also, the addition of marking 'must/should/may' as well as Figure 2 have greatly improved its accessibility and, frankly, usefulness.

My remaining suggestion is to add a 'housekeeping sentence' - perhaps in the last section of the Introduction: Pointing out 'how this paper works' right at the start, that 'must/should/would' will be marked, and that an overwiev figure and a list of abbreviations is available to refer back to, might avoid some frustration for those who are willing but slightly overwhelmed.
If you decide against such a sentence, I would still recommend to reference Figure 2 as early as possible.
