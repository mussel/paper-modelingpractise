% SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
% SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
% SPDX-License-Identifier: CC0-1.0
%
\documentclass[APA,LATO2COL]{arxiv}

$if(logo)$
\usepackage{tikz}
$endif$

\usepackage{longtable}
\makeatletter
\let\oldlt\longtable
\let\endoldlt\endlongtable
\def\longtable{\@ifnextchar[\longtable@i \longtable@ii}
\def\longtable@i[#1]{\begin{figure}[t]
\onecolumn
\begin{minipage}{0.5\textwidth}
\oldlt[#1]
}
\def\longtable@ii{\begin{figure}[t]
\onecolumn
\begin{minipage}{0.5\textwidth}
\oldlt
}
\def\endlongtable{\endoldlt
\end{minipage}
\twocolumn
\end{figure}}
\makeatother

$if(csl-refs)$
% definitions for citeproc citations
\NewDocumentCommand\citeproctext{}{}
\NewDocumentCommand\citeproc{mm}{%
\begingroup\def\citeproctext{#2}\cite{#1}\endgroup}
\makeatletter
% allow citations to break across lines
\let\@cite@ofmt\@firstofone
% avoid brackets around text for \cite:
\def\@biblabel#1{}
\def\@cite#1#2{{#1\if@tempswa , #2\fi}}
\makeatother
\newlength{\cslhangindent}
\setlength{\cslhangindent}{1.5em}
\newlength{\csllabelwidth}
\setlength{\csllabelwidth}{3em}
\newenvironment{CSLReferences}[2] % #1 hanging-indent, #2 entry-spacing
{\begin{list}{}{%
	\setlength{\itemindent}{0pt}
	\setlength{\leftmargin}{0pt}
	\setlength{\parsep}{0pt}
	% turn on hanging indent if param 1 is 1
	\ifodd #1
	\setlength{\leftmargin}{\cslhangindent}
	\setlength{\itemindent}{-1\cslhangindent}
	\fi
	% set entry spacing
	\setlength{\itemsep}{#2\baselineskip}}}
{\end{list}}
\newcommand{\CSLBlock}[1]{\hfill\break\parbox[t]{\linewidth}{\strut\ignorespaces#1\strut}}
\newcommand{\CSLLeftMargin}[1]{\parbox[t]{\csllabelwidth}{\strut#1\strut}}
\newcommand{\CSLRightInline}[1]{\parbox[t]{\linewidth - \csllabelwidth}{\strut#1\strut}}
\newcommand{\CSLIndent}[1]{\hspace{\cslhangindent}#1}
$endif$

\usepackage{graphicx}
 \makeatletter
 \newsavebox\pandoc@box
 \newcommand*\pandocbounded[1]{% scales image to fit in text height/width
   \sbox\pandoc@box{#1}%
   \Gscale@div\@tempa{\textheight}{\dimexpr\ht\pandoc@box+\dp\pandoc@box\relax}%
   \Gscale@div\@tempb{\linewidth}{\wd\pandoc@box}%
   \ifdim\@tempb\p@<\@tempa\p@\let\@tempa\@tempb\fi% select the smaller of both
   \ifdim\@tempa\p@<\p@\scalebox{\@tempa}{\usebox\pandoc@box}%
   \else\usebox{\pandoc@box}%
   \fi%
 }
 % Set default figure placement to htbp
 \def\fps@figure{htbp}
 \makeatother

\articletype{$if(articletype)$$articletype$$else$Preprint$endif$}
\received{$if(received)$$received$$else$pending$endif$}
\revised{$if(revised)$$revised$$else$pending$endif$}
\accepted{$if(accepted)$$accepted$$else$pending$endif$}
\published{$if(published)$$published$$else$pending$endif$}
\journal{%
$if(published)$Published in$else$$if(accepted)$Accepted by$else$$if(revised)$Revised for$else$$if(submitted)$Submitted to$endif$$endif$$endif$$endif$
$if(journal)$$journal$$else$Arxiv$endif$
}

%\volume{$if(volume)$$volume$$else$$endif$}
\volume{$if(volume)$$volume$$else$$if(published)$$published$$else$$if(accepted)$$accepted$$else$$if(revised)$$revised$$else$$if(submitted)$$submitted$$endif$$endif$$endif$$endif$$endif$}

\copyyear{$if(copyyear)$$copyyear$$else$2024$endif$}

\startpage{1}
\raggedbottom

\begin{document}

\title{$title$}
\titlemark{$if(runningtitle)$$runningtitle/uppercase$$else$$title/uppercase$$endif$}

%$if(author.orcid)$\href{https://orcid.org/$author.orcid$}{\orcidicon}$endif$
$for(author)$
\author[$author.affil$]{$author.name$}
$sep$
$endfor$

% $for(author)$
% $if(author/first)$
% \authormark{$author.name/uppercase$ \textsc{et al.}}
% $endif$
% $endfor$

\authormark{$runningauthor$}

$for(address)$
\address[$address.affil$]{\orgdiv{$address.department$},
\orgname{$address.institution$},
\orgaddress{$address.street$, \state{$address.state$}, \country{$address.country$}}}
$sep$
$endfor$

\corres{
$for(author)$
$if(author.email)$
$author.name$, \email{$author.email$}
$endif$
$sep$
$endfor$}
%\presentaddress{This is sample for present address text this is sample for present address text.}
$if(funding)$
\fundingInfo{$funding$}
$endif$

$if(abstract)$
\abstract[Abstract]{$abstract$}
$endif$

% Keywords
$if(keywords)$
\keywords{$keywords$}
$endif$
%\keywords{keyword1, keyword2, keyword3, keyword4}

%\jnlcitation{\cname{%
%\author{Taylor M.},
%\author{Lauritzen P},
%\author{Erath C}, and
%\author{Mittal R}}.
%\ctitle{On simplifying ‘incremental remap’-based transport schemes.} \cjournal{\it J Comput Phys.} \cvol{2021;00(00):1--18}.}

\maketitle

$if(abbreviations)$
\renewcommand\thefootnote{}
\footnotetext{\textbf{Abbreviations:}
$for(abbreviations)$$abbreviations.short$, $abbreviations.long$; $sep$$endfor$
}
\renewcommand\thefootnote{\fnsymbol{footnote}}
\setcounter{footnote}{1}
$endif$

$if(logo)$
\begin{tikzpicture}[remember picture,overlay,shift={(current page.north east)}]
\node[anchor=north east,xshift=-1cm,yshift=-1cm]{\includegraphics[width=5cm]{omf_logo.png}};
\end{tikzpicture}
$endif$

$body$

%---------------------------------
%\backmatter

\bmsection*{Author contributions}
$if(authorcontributions)$
$authorcontributions$
$else$
All authors contributed equally to this work.
$endif$

$if(acknowledgement)$
\bmsection*{Acknowledgments}
$acknowledgement$
$endif$

\bmsection*{Financial disclosure}
$if(disclosure)$$disclosure$$else$None reported.$endif$

\bmsection*{Conflict of interest}
$if(conflictsofinterest)$$conflictsofinterest$$else$The authors declare no potential conflict of interests.$endif$

$if(bibliography)$
%\reftitle{References}
%\externalbibliography{yes}
\bibliography{$bibliography$}
$endif$

\end{document}
